package tech.mastertech.itau.produto.repositories;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import tech.mastertech.itau.produto.models.Fornecedor;

public interface FornecedorRepository extends CrudRepository<Fornecedor, Integer> {
  public Optional<Fornecedor> findByNomeUsuario(String nomeUsuario);
}
