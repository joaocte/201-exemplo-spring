package tech.mastertech.itau.produto.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import tech.mastertech.itau.produto.models.Fornecedor;
import tech.mastertech.itau.produto.security.JwtTokenProvider;
import tech.mastertech.itau.produto.services.FornecedorService;

@RestController
public class LoginController {
  
  @Autowired
  private FornecedorService fornecedorService;

  @PostMapping("/login")
  public Map<String, Object> login(@RequestBody Fornecedor fornecedor) {
    Fornecedor fornecedorSalvo = fornecedorService.fazerLogin(fornecedor);
    
    if(fornecedorSalvo == null) {
      throw new ResponseStatusException(HttpStatus.FORBIDDEN);
    }
    
    Map<String, Object> retorno = new HashMap<>();
    retorno.put("fornecedor", fornecedorSalvo);
    
    JwtTokenProvider provider = new JwtTokenProvider();
    String token = provider.criarToken(fornecedor.getNomeUsuario());
    retorno.put("token", token);
    
    return retorno;
  }
}
