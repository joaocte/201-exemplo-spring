package tech.mastertech.itau.produto.controllers;

import static org.mockito.Mockito.verifyZeroInteractions;
//import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.assertj.core.util.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;

import tech.mastertech.itau.produto.models.Categoria;
import tech.mastertech.itau.produto.models.Produto;
import tech.mastertech.itau.produto.services.CategoriaService;
import tech.mastertech.itau.produto.services.ProdutoService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = ProdutoController.class)
public class ProdutoControllerTest {
  @Autowired
  private MockMvc mockMvc;
  
  @MockBean
  private ProdutoService produtoService;
  @MockBean
  private CategoriaService categoriaService;
  
  private ObjectMapper mapper = new ObjectMapper();
  private int id = 1;
  private Categoria categoria;
  private Produto produto;
  
  @Before
  public void preparar() {
    categoria = new Categoria();
    categoria.setNome("Álcool");
    
    produto = new Produto();
    produto.setId(id);
    produto.setNome("Pinga");
    produto.setCategoria(categoria);
  }
  
  @Test
  public void deveBuscarUmProdutoPorId() throws Exception {
    when(produtoService.getProduto(id)).thenReturn(produto);
    
    mockMvc.perform(get("/produto/" + id))
            .andExpect(status().isOk())
            .andExpect(content().string(mapper.writeValueAsString(produto)));
//            .andExpect(content().string(containsString(String.valueOf(produto.getId()))))
//            .andExpect(content().string(containsString(produto.getNome())))
//            .andExpect(content().string(containsString(categoria.getNome())));
  }
  
  @Test
  public void deveBuscarTodosOsProdutos() throws Exception { 
    List<Produto> produtos = Lists.list(produto);
    
    when(produtoService.getProdutos()).thenReturn(produtos);
    
    mockMvc.perform(get("/produtos"))
            .andExpect(status().isOk())
            .andExpect(content().string(mapper.writeValueAsString(produtos)));
    
    verifyZeroInteractions(categoriaService);
  }
  
  @Test
  public void deveBuscarTodosOsProdutosDeUmaCategoria() throws Exception {
    List<Produto> produtos = Lists.list(produto);
    
    when(produtoService.getProdutos(categoria)).thenReturn(produtos);
    when(categoriaService.getCategoriaByNome(categoria.getNome())).thenReturn(Optional.of(categoria));
    
    mockMvc.perform(get("/produtos?nome=" + categoria.getNome()))
            .andExpect(status().isOk())
            .andExpect(content().string(mapper.writeValueAsString(produtos)));
  }
  
  @Test
  public void deveRetornarVetorVazionQuandoUmaCategoriaNaoExiste() throws Exception {
    List<Produto> produtos = Lists.list(produto);
    
    when(produtoService.getProdutos(categoria)).thenReturn(produtos);
    when(categoriaService.getCategoriaByNome(categoria.getNome())).thenReturn(Optional.empty());
    
    mockMvc.perform(get("/produtos?nome=" + categoria.getNome()))
            .andExpect(status().isOk())
            .andExpect(content().string("[]"));
  }
  
  @Test
  @WithMockUser
  public void deveAlterarValorDeUmProduto() throws Exception {
    double valor = 1.0;
    produto.setValor(valor);
    
    when(produtoService.mudarValor(id, valor)).thenReturn(produto);
    
    Map<String, Double> payload = new HashMap<>();
    payload.put("valor", valor);
    
    String produtoJson = mapper.writeValueAsString(produto);
    String payloadJson = mapper.writeValueAsString(payload);
    
    mockMvc.perform(
        patch("/auth/produto/" + id + "/valor")
        .content(payloadJson)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
    )
    .andExpect(status().isOk())
    .andExpect(content().string(produtoJson));
  }
  
  @Test
  @WithMockUser
  public void deveRetornarErroQuandoValorENulo() throws Exception {
    double valor = 1.0;
    
    when(produtoService.mudarValor(id, valor)).thenReturn(produto);
    
    Map<String, Double> payload = new HashMap<>();
    
    String payloadJson = mapper.writeValueAsString(payload);
    
    mockMvc.perform(
        patch("/auth/produto/" + id + "/valor")
        .content(payloadJson)
        .contentType(MediaType.APPLICATION_JSON_UTF8)
    )
    .andExpect(status().isBadRequest());
  }
  
}
